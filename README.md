# **Digital and Soft Skills - Challenge**
## **Object 2D orientation using aruco marker and openCV**
### Noureldeen Nagm 
<br>

-- --

### **Pose estimation - Aruco marker**  

There are many openCV repos that get the angle of orientation for all the objects in the camera view. The special thing here is using an aruco marker attached to the objects surface we will get the orientation of only one specific object.

[Tutorial page](https://automaticaddison.com/how-to-perform-pose-estimation-using-an-aruco-marker/?fbclid=IwAR0VIGb5652RN-BfWC9RhPV-FMH7rg6RUuLkfYpY1_0VgGy-CVANSgJtFmo)  

![01_Aruco-pose-detection.JPG](./media/01_Aruco-pose-detection.JPG) 

**Prerequisites**  
- Aruco marker.
- Open CV (python).
- Scipy.
- Built in or external webcam.

**Creating an Aruco marker** 
[Aruco marker generator Link](https://chev.me/arucogen/)  
- Dictionary: Original ArUco.
- Marker ID: 1.
- Marker Size, mm: 100.

![02_Ar_generator.JPG](./media/02_Ar_generator.JPG) 

**Setting up anaconda environment** 

1.) Create a python 3.8 anaconda invironment an name it "Aruco".

![03_Conda.JPG](./media/03_Conda.JPG) 

2.) Install openCV by Opening the Terminal and typing the command:
```
pip install opencv-contrib-python
```
3.) Install scipy by Opening the Terminal and typing the command:
```
pip install pip install scipy
``` 

**Camera callibration**   

1.) Print a chess board on A4 paper.
[Chessboard image Link](https://github.com/opencv/opencv/blob/master/doc/pattern.png)  
2.) Take as many photos of the printed chessboard from different angles and distances.
Here is an example of a picture I took from my built in camera.

![04_Chess.jpg](./media/04_Chess.jpg) 

3.) Save the photos in the same folder directory you will save the code in.

4.) Download the bellow code and save it in the same directory with the photos of the chessboard.

[camera_calibration.py](./core/camera_calibration.py)  

5.) Move to the directory through the terminal and run the code by typing the bellow command:
```
Python camera_calibration.py
```
This should open a window that looks like this:

![05_callibration.JPG](./media/05_callibration.JPG) 

and will create a yaml file in the folder directory that looks like this:

![06_yaml.JPG](./media/06_yaml.JPG) 

**Running the code** 

1.) Download the bellow code and save it in the same directory.

[aruco_marker_pose_estimator.py](./core/aruco_marker_pose_estimator.py)  

2.) Run the code by typing the bellow command in the terminal:
```
Python aruco_marker_pose_estimation.py
```
The output should look like this:

![07_problems.JPG](./media/07_problems.JPG)  

**Problems with the output** 

1.) As you can see in the above photo the result is an orientation in the x (roll), y (pitch) and z (yaw) and we only need the yaw for 2D orientation.

2.) The yaw has only a range from 0 to 180, so any angle above 180 will have a value of negative. 

3.) The results are printed directly with every movement and we need to slow it down in order to process the results.

**Code modifications** 

1.) In order to print only the yaw z we delete the lines that print the other dimensions and rename yaw z to angle of orientation.
```
print("roll_x: {}".format(roll_x))
        print("pitch_y: {}".format(pitch_y))
```
2.) In order to mape the negative results to positive we create a simple mathimatical formula that adds 360 to any negative result.
```
if (yaw_z <0):
          print ("angle of orientation: {}".format(yaw_z+360))
        else:
           print("angle of orientation: {}".format(yaw_z))
```       
3.) In order to create a break of 2 seconds between results we add a python sleep formula by imort time at the start of the code.
```
import time 
```
and adding this line of code after the print line.
```
time.sleep(2)
```
Full code after modifications: [pose_estimator_modified.py](./core/pose_estimator_modified.py)  

What you get by running the code should be something like this:

![08_final-result.JPG](./media/08_final-result.JPG) 

<br>

-- --
contact information: noureldeen.nagm@rwth-aachen.de - Noureldeen Nagm